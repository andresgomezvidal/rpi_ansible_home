The objective of this repository is to serve as an automatization/record of the settings for my personal use and as an example for anyone interested. In this case there is only a raspberry pi 4 (only a host), but this should be easy to adapt to as many as needed. Some settings are set with config files from a previous rpi3 setup.


The ideal order should be something like:

	#edit the hosts, variables in group_vars/all.yml and the playbooks to suit your needs
	#it is assumed that the fingerprint is in the known_hosts file

    ansible-playbook tasks/ansible_dependencies.yml -K
    ansible-playbook -i hosts tasks/users_ssh.yml
    ansible-playbook tasks/upgrade.yml
    ansible-playbook tasks/fail2ban.yml
    ansible-playbook tasks/programs.yml
    ansible-playbook tasks/static_ip.yml 	#reboot (automated)

    ansible-playbook tasks/pihole.yml
    ansible-playbook tasks/fstab.yml
    ansible-playbook tasks/logrotate.yml
	#recommended reboot

	#could give trouble (the test went well, but I don't trust them much)
    ansible-playbook tasks/firewall.yml

    ansible-playbook tasks/raspicast.yml
    ansible-playbook tasks/docker.yml
    ansible-playbook tasks/jellyfin.yml
    ansible-playbook tasks/kodi.yml

    ansible-playbook tasks/influxdb.yml
    ansible-playbook tasks/grafana.yml
    ansible-playbook tasks/netdata.yml
    ansible-playbook tasks/sshfs_email.yml

	#these are kind of miscellaneous
    ansible-playbook tasks/disable_swapfile.yml
    ansible-playbook tasks/disable_wifi_bluetooth.yml
    ansible-playbook tasks/mouse_position.yml
    ansible-playbook tasks/screensaver.yml
    ansible-playbook tasks/vnc_server.yml
    ansible-playbook tasks/steamlink.yml
    ansible-playbook tasks/mcomix.yml


    ansible-playbook tasks/cleanup.yml




Even with new users is annoying to type the password (for -K, --ask-become-pass) and it does not make sense to change their sudoers settings for lesser security, instead I just keep using the `pi` user until the process is complete.

I could have used more templates with Jinja2, but I didn't have much time.
