#!/usr/bin/env bash

LOCK=/tmp/controller_detected_steamlink.lock

while true; do
	if [[ -n "$(ls -al /dev/snd/by-path/ |grep -E 'platform-fd500000.pcie-pci-0000:01:00.0-usb-0:1.3:1.0|platform-fd500000.pcie-pci-0000:01:00.0-usb-0:1.1.2:1.0|3f980000.usb-usb')" ]]; then
		if [[ ! -e "$LOCK" && -z "$(ps -aux|grep steamlink|grep -v grep|grep -v `basename -- $0`)" ]]; then
			touch "$LOCK"
			export DISPLAY=:0; steamlink &> /dev/null
		fi
	else
		rm -f "$LOCK"
	fi
	sleep 10
done
