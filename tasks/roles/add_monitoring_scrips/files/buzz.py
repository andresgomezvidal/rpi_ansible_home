import RPi.GPIO as GPIO
import time
#import signal
#import atexit

BuzzerPin = 3

#def silenceBuzzer():
#    GPIO.output(BuzzerPin, GPIO.LOW)

#atexit.register(silenceBuzzer)
#signal.signal(signal.SIGTERM, silenceBuzzer)
#signal.signal(signal.SIGINT, silenceBuzzer)


GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(BuzzerPin, GPIO.OUT, initial=GPIO.LOW)

for i in range(0,5):
    GPIO.output(BuzzerPin, GPIO.HIGH)
    time.sleep(0.05)
    if i%2==0:
        GPIO.output(BuzzerPin, GPIO.LOW)
        time.sleep(0.35)
        GPIO.output(BuzzerPin, GPIO.HIGH)
        time.sleep(0.05)
        GPIO.output(BuzzerPin, GPIO.LOW)
        time.sleep(0.35)
        GPIO.output(BuzzerPin, GPIO.HIGH)
        time.sleep(0.05)
    GPIO.output(BuzzerPin, GPIO.LOW)
    print("buzzed")
    time.sleep(10)
