#!/usr/bin/env bash

while true; do
	if [[ -n "$(ls -al /dev/input/by-id/ 2> /dev/null |grep 'usb-Gasia_Co._Ltd_PS_R__Gamepad-event-joystick')" ]]; then
		if [[ -z "$(ps -aux|grep mcomix|grep -v grep|grep -v `basename -- $0`)" ]]; then
			export DISPLAY=:0
			if [[ -z "$(ps -aux|grep qjoypad|grep -v grep|grep -v `basename -- $0`)" ]]; then
				qjoypad &> /dev/null &
			fi
			xrandr --output HDMI-1 --mode 1920x1080
			mcomix &> /tmp/mcomix.log &
		fi
	else
		#when the controller is removed the programs are killed (mcomix saves the last position read)
		killall -q -I mcomix
		killall -q -I qjoypad
	fi
	sleep 10
done
