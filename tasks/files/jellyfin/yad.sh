#!/usr/bin/env bash

while [ -n "$(ps -aux|grep yad|grep -v grep|grep text|grep span)" ]; do
	sleep 2
done
export DISPLAY=:0
yad --timeout $1 --skip-taskbar --center --no-buttons --text-align=center --undecorated --text="<span color=\"#ffffff\" bgcolor=\"#000000\" font_size=\"xx-large\" font_weight=\"heavy\">   $2   </span>" &> /dev/null
