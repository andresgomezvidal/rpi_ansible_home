#!/usr/bin/env bash

my_pid=$$
parent_pid=$(ps --no-header -ao pid,ppid| awk '$1=="'$my_pid'" {print $2}') 				#cannot use $$ because subshell
if [ -z "$parent_pid" ]; then
	parent_pid=1
fi
check_existing="$(ps --no-header -ao pid,args|awk '$2~/bash \/root\/check_media-center_jellyfin.sh/ && $2!~/awk/ && $2!~/cron/ && $1!="'$$'" && $1!="'$parent_pid'" {print $0}')"
if [ -n "$check_existing" ]; then
	echo "A process is already running"
	echo -n "$check_existing"
	echo "my ppid is $parent_pid"
	echo "my pid is $$"
	exit 1
fi

#trap 'echo >/dev/null' SIGUSR1 			#kill -USR1 pid #interrupts sleep


sname='docker-jellyfin_server.service'
LOG=/tmp/check_media-center.log
LOCK_DETACH=/tmp/media-center.LOCK_DETACH
LOCK_MOUNT=/tmp/media-center.LOCK_MOUNT
#UUID="7893b566-4106-442c-96e9-2335aa18de80" #DISCO 6TB
UUID="f5ce063f-0159-4d31-9a33-4d87481c3744" #DISCO 4TB
#UUID="e24b2c72-6757-4bde-9759-b2a937fc06f8" #DISCO 2TB
tries_detach=0
tries_docker=0
tries_kill_kodi=0
last_umounted=0
last_connected=0
run=0

log_f(){ echo -e "$(date +%Y/%m/%d--%H:%M:%S)\t\t$1" >> "$LOG"; }
yad_f(){
	local seconds=5
	[[ -n "$2" ]] && seconds=$2
	sudo -u erwin /home/erwin/yad.sh $seconds "$1"
	log_f "$1"
}
is_cec_on_f(){ echo pow 0 |cec-client -s -d 1 |awk '/power status: on/ {print $1}'; }
get_status_f(){ systemctl status $sname| awk 'NR==3 {print $2}'; }
get_lsof_f(){ lsof 2>/dev/null |grep '/media-center'; }
detach_f(){
	yad_f 'Desmontando disco...' 5 &
	#local disk="$(ls -al /dev/disk/by-uuid/$UUID |awk -F '/' '{print $NF}')"
	#umount /media-center && udisks --detach /dev/"${disk:0:3}" && yad_f 'Se puede apagar el disco' 20
	umount /media-center && hdparm -Y /dev/sda sleep 5 && yad_f 'Se puede apagar el disco' 20
}

while true; do
	check_uuid="$(ls -al /dev/disk/by-uuid|grep $UUID)"
	check_mounted="$(mount|grep '/media-center')"
	check_kodi="$(ps --no-header -aux |grep '/usr/bin/kodi' |grep -v 'grep')"
	check_lsof='null'

	if [ -z "$kodi_pid" ]; then
		##START TESTING
		if [[ -n "$check_uuid" && -z "$check_mounted" && ! -e "$LOCK_MOUNT" && "$last_connected" != "$(\ls -al /dev/disk/by-uuid/$UUID |awk '{print $5, $6, $7, $8}')" ]]; then
			last_connected="$(\ls -al /dev/disk/by-uuid/$UUID |awk '{print $5, $6, $7, $8}')"
			tries_detach=0
			mount -t ext4 -o noatime,rw /dev/disk/by-uuid/"$UUID" /media-center
			yad_f 'Montando disco...'
		elif [[ ! -e "$LOCK_DETACH" && -n "$check_mounted" && -z "$check_kodi" ]]; then
			[[ "$check_lsof" = 'null' ]] && check_lsof="$(get_lsof_f)"
			if [[ -z "$check_lsof" ]]; then
				if [ "$tries_detach" -lt 15 ]; then
					tries_detach=$((tries_detach + 1))
				else
					tries_detach=0
					detach_f
				fi
			fi
		else
			tries_detach=0
		fi
		##FINISH

		if [[ "$(get_status_f)" = "active" ]]; then
			if [[ ! -e "$LOCK_DETACH" && ( -z "$check_mounted" || -z "$check_uuid" ) ]]; then
				[[ "$check_lsof" = 'null' ]] && check_lsof="$(get_lsof_f)"
				if [[ -z "$check_lsof" ]]; then
					if [ "$tries_docker" -lt 12 ]; then
						tries_docker=$((tries_docker + 1))
					else
						tries_docker=0
						log_f "systemctl stop $sname"
						systemctl stop $sname &> /dev/null
					fi
				else
					tries_docker=0
				fi
			else
				tries_docker=0
			fi
		elif [[ -n "$check_mounted" ]]; then
			tries_docker=0
			log_f "systemctl start $sname"
			systemctl start $sname &> /dev/null
		fi

		if [[ -z "$check_mounted" ]]; then
			last_umounted="$(date +%s)"
		fi

		if [[ -n "$check_mounted" && "$(($(date +%s) - last_umounted))" -le 60 && -z "$check_kodi" && -n "$(is_cec_on_f)" ]]; then
			yad_f 'Lanzando Kodi...' 25 &
			sleep 25
			echo "1" > /dev/udp/127.0.0.1/5600
			echo "1" > /dev/udp/127.0.0.1/5600
			sleep 5
		fi

		if [[ -n "$check_kodi" ]]; then
			kodi_pid="$(echo "$check_kodi"| awk '{print $2}')"
			tries_detach=0
			tries_docker=0
			tries_kill_kodi=0
		fi
	elif [[ -z "$check_kodi" ]]; then
		unset kodi_pid
		if [[ ! -e "$LOCK_DETACH" && -n "$check_mounted" ]]; then
			[[ "$check_lsof" = 'null' ]] && check_lsof="$(get_lsof_f)"
			if [[ -z "$check_lsof" ]]; then
				detach_f
			fi
		fi
	elif [[ -z "$check_mounted" ]]; then 				#in case the disk powers off without closing kodi
		[[ "$check_lsof" = 'null' ]] && check_lsof="$(get_lsof_f)"
		if [[ -z "$check_lsof" ]]; then
			if [ "$tries_kill_kodi" -lt 30 ]; then
				tries_kill_kodi=$((tries_kill_kodi + 1))
			else
				log_f "Killing kodi: /media-center not mounted"
				kill $kodi_pid
				kodi_pid2="$(ps --no-header -aux |awk '$0~/\/usr\/lib\/arm-linux-gnueabihf\/kodi\/kodi/ {print $2}')"
				kill $kodi_pid2
				sleep 5
				if [ -n "$(ps --no-header -aux |awk '$0~/\/usr\/bin\/kodi/ {print $2}')" ]; then
					kill -9 "$(ps --no-header -aux |awk '$0~/\/usr\/bin\/kodi/ {print $2}')"
				fi
				if [ -n "$(ps --no-header -aux |awk '$0~/\/usr\/lib\/arm-linux-gnueabihf\/kodi\/kodi/ {print $2}')" ]; then
					kill -9 "$(ps --no-header -aux |awk '$0~/\/usr\/lib\/arm-linux-gnueabihf\/kodi\/kodi/ {print $2}')"
				fi
				tries_kill_kodi=0
			fi
		fi
	else
		tries_detach=0
		tries_docker=0
		tries_kill_kodi=0
	fi

	sleep 10 # & wait $!
done
