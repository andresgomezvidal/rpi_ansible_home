## a bit of bash to find not mentioned .yml and not used files

(cd tasks; for i in *.yml; do if [ -z "$(grep $i ../README.me)" ]; then echo "missing $i in README.me" ; fi; done; )

find tasks/files -type f |while read -r i; do
	not_found="$i"
	while read -r j; do
		if [[ -n "$(\grep "${j:1}" <<< "${i:12}")" ]]; then
			not_found=""
			break
		fi
	done <<< "$(\grep 'src:' tasks/*.yml |awk -F 'src:' '{$2=substr($2,2);print $2}')"
	if [ -n "$not_found" ]; then
		echo "file not used in any .yml: $not_found"
	fi
done


##check if every .yml mentions itself
for i in tasks/*.yml; do
	if [ -z "$(grep ${i:6} <(head -1 $i))" ]; then
		echo "incorrect first line: $i"
	fi
done
