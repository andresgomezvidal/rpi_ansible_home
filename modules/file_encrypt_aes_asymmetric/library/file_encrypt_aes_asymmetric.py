#!/usr/bin/python3

#https://blog.toast38coza.me/custom-ansible-module-hello-world/
#https://docs.ansible.com/ansible/latest/dev_guide/developing_module_utilities.html

from ansible.module_utils.basic import *
import os.path
import hashlib
import secrets     #python 3.6
import string


from subprocess import Popen, PIPE #to wrap it up with openssl instead of a proper python module


#https://pypi.org/project/pyAesCrypt/
#the best way would be to install it with pip
#import pyAesCrypt
import sys #workaround because import doesn't work inside an ansible module
sys.path.append('./library/pyAesCrypt')
from crypto import encryptFile, decryptFile, encryptStream, decryptStream



def encrypt_file(source, destination, symmetric_key):
    if not symmetric_key or symmetric_key == "0":
        alphabet = string.ascii_letters + string.digits
        symmetric_key   = ''.join(secrets.choice(alphabet) for i in range(60))

    #pyAesCrypt.encryptFile(source, destination, symmetric_key)
    encryptFile(source, destination, symmetric_key) #workaround because import doesn't work inside an ansible module

    return symmetric_key

def decrypt_file(source, destination, symmetric_key):
    #pyAesCrypt.decryptFile(source, destination, symmetric_key)
    decryptFile(source, destination, symmetric_key) #workaround because import doesn't work inside an ansible module

    return ""


#check if asymmetric_key_file is defined, if not then tries the asymmetric_key_folder and asymmetric_key_md5sum combination
def asymmetric_file_choose(asymmetric_key_file, asymmetric_key_folder, asymmetric_key_md5sum):
    if asymmetric_key_file and asymmetric_key_file != "0":
        return asymmetric_key_file

    if not asymmetric_key_folder or asymmetric_key_folder == "0" or not asymmetric_key_md5sum or asymmetric_key_md5sum == "0" or not os.path.exists(asymmetric_key_folder):
        return ""

    for root, dirs, files in os.walk(asymmetric_key_folder):
        for name in files:
            filepath=root+os.sep+name
            if hashlib.md5(open(filepath,'rb').read()).hexdigest() == asymmetric_key_md5sum:
                return filepath
    return ""





#it was border limit to workaround importing pyAesCrypt but rsa modules like pyCryptodome are bigger and harder to import, so this is the point where usually we would do something else from scratch,
# like an independent pythong script, and if possible something with low level access to memory in order to wipe out the condifential data
#so I am just going to use openssl and move to another project
#we can encrypt the symmetric password with the asymmetric ssh public key like this:
# openssl pkeyutl -encrypt -inkey <publicKeyFile> -pubin -out <encryptedFile> <<< <symmetricKeyString>
#and decrypt the previous file with the asymmetric ssh private key:
# openssl pkeyutl -decrypt -in /tmp/salida2 -inkey rsa_key

def encrypt_symmetric_key(symmetric_key, asymmetric_file, symmetric_key_encrypted_file):
    p = Popen('openssl pkeyutl -encrypt -inkey '+asymmetric_file+' -pubin -out '+symmetric_key_encrypted_file+' <<< '+symmetric_key, shell=True, stdout=PIPE, stderr=PIPE)


def main():
    fields = {
          "source": {"required": True, "type": "str"},                                   #file to encrypt
          "destination": {"required": True, "type": "str" },                             #path for the encrypted file
          "symmetric_key": {"required": False, "type": "str"},                           #symmetric key
          "symmetric_key_encrypted_file": {"required": True, "type": "str"},             #file where the encrypted symmetric_key is saved into
          "asymmetric_key_file": {"required": False, "type": "str"},                     #asymmetric key file to use for the asymmetric key
          "asymmetric_key_folder": {"required": False, "type": "str"},                   #search folder for asymmetric keys, md5sum them and check if any asymmetric_key_md5sum fits
          "asymmetric_key_md5sum": {"required": False, "type": "str" },                  #  both asymmetric_key_folder and asymmetric_key_md5sum have to be defined
          "print_symmetric_key": {"default": False, "type": "bool" },                     #returns the key string to ansible
          "action": {
            "default": "encrypt",
            "choices": ['encrypt', 'decrypt'],
            "type": 'str'
          },
    }
    module = AnsibleModule(argument_spec=fields)
    ansible_changed=False

    asymmetric_file=asymmetric_file_choose(module.params['asymmetric_key_file'], module.params['asymmetric_key_folder'], module.params['asymmetric_key_md5sum'])
    if not asymmetric_file or asymmetric_file == "0":
        module.exit_json(changed=ansible_changed, meta="No valid asymmetric file was introduced", failed=True)
    elif not os.path.exists(asymmetric_file):
        module.exit_json(changed=ansible_changed, meta=asymmetric_file+" Is not a valid asymmetric file", failed=True)

    if module.params['source'] == module.params['destination']:
        module.exit_json(changed=ansible_changed, meta="Source and destionation are the same file "+source, failed=True)


    if module.params['action'] == "decrypt":
        if not module.params['symmetric_key'] or module.params['symmetric_key'] == "0": #if we passed symmetric_key as an argument
            module.exit_json(changed=ansible_changed, meta="The symmetric_key is not defined", failed=True)
        symmetric_key=decrypt_file(module.params['source'], module.params['destination'], module.params['symmetric_key'])
    else:
        symmetric_key=encrypt_file(module.params['source'], module.params['destination'], module.params['symmetric_key'])
    ansible_changed=True


    encrypt_symmetric_key(symmetric_key, asymmetric_file, module.params['symmetric_key_encrypted_file'])

    if not module.params['print_symmetric_key']:
        symmetric_key=""

    module.exit_json(changed=ansible_changed, meta=symmetric_key)






















if __name__ == '__main__':
    main()
